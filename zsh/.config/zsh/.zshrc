# Setup history
HISTSIZE=10000
SAVEHIST=1000
HISTFILE=~/.cache/zsh/history

# Make prompt pretty
PS1='%B%F{240}[%f%F{#7B00C7}%n%f%F{22}@%f%F{26}%m%f%F{88} %~%f%F{240}]%f%b%F{184} %#%f%F{white}%f '

# not Vim mode
bindkey -e

# Aliases
alias tmux="tmux -uf ~/.config/tmux/tmux.conf"
alias ls="ls -B --color --group-directories-first"
alias /s="ls -B --color --group-directories-first"
alias /-="ls -B --color --group-directories-first"
alias ls-="ls -B --color --group-directories-first"
alias cmatrix="cmatrix -ab"
alias ed="ed -v -p:"
alias roll="mpv --fullscreen /home/aj/vids/roll.mkv"
alias s="setsid -f"
#alias vim='vim -c "let g:tty='\''$(tty)'\''"'
alias gotop='gotop -c monokai'
alias tsm='transmission-remote'
alias logout='clear && logout'
alias lpq='lpq -a'

function pdfman(){
	man -Tpdf "$1" | zathura - & disown
}

############### Completion ###############

zmodload -i zsh/complist

# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-/'\''"`]=** r:|=**'
zstyle ':completion:*' menu select
zstyle ':completion:*' original false
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/aj/.config/zsh/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

zstyle ':completion:*' list-colors ''

source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
