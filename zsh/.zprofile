#!/bin/zsh

export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export EDITOR="nvim"
export READER="zathura"
export PASSWORD_STORE_GENERATED_LENGTH="24"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin:/usr/lib/llvm/11/bin:/home/aj/.scripts:/home/aj/.go/bin:/home/aj/.local/bin:/home/aj/.cargo/bin:/home/aj/.local/share/gem/ruby/2.7.0/bin:/home/aj/.node_modules/bin:/home/aj/builds/appimages"
export $(dbus-launch)
export VULTR_API_KEY=C4B6BWNFJL6AZIDHDF2QRKU5IW6X4O7D25OA
# Disable CSD for gtk stuff
export GTK_CSD=0
export LD_PRELOAD="/usr/local/lib/libgtk3-nocsd.so.0"
